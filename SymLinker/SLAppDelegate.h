//
//  SLAppDelegate.h
//  SymLinker
//
//  Created by Joss Gray on 09/01/2012.
//  Copyright (c) 2012 none. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SLAppDelegate : NSObject <NSApplicationDelegate>{

NSURL *sourceURL;
NSURL *targetURL;
NSString *sourcePath;
NSString *targetPath;
NSTask *symLinkTask;
}

@property (assign) IBOutlet NSWindow *window;
@property (retain) NSURL *sourceURL;
@property (retain) NSURL *targetURL;
@property (retain) NSString *sourcePath;
@property (retain) NSString *targetPath;
@property (assign) IBOutlet NSTextField *sourceField;
@property (assign) IBOutlet NSTextField *targetField;

-(IBAction)chooseSource:(id)sender;

-(IBAction)chooseTarget:(id)sender;

-(IBAction)createSymLink:(id)sender;

-(void)initTask;

-(NSString*)urlToStringPath:(NSURL*) input;

-(void)chooseSource:(id)sender;

-(void)chooseTarget:(id)sender;

@end
