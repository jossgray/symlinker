//
//  SLAppDelegate.m
//  SymLinker
//
//  Created by Joss Gray on 09/01/2012.
//  Copyright (c) 2012 none. All rights reserved.
//

#import "SLAppDelegate.h"

@implementation SLAppDelegate

@synthesize window;
@synthesize sourceURL;
@synthesize targetURL;
@synthesize sourcePath;
@synthesize targetPath;
@synthesize sourceField;
@synthesize targetField;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self initTask];
}

-(void)initTask{
    symLinkTask = [NSTask new];
    [symLinkTask setLaunchPath:@"/bin/ln"];
}

-(NSString*)urlToStringPath:(NSURL*) input{
    NSString* path = [input path];
    return path;
    
}

-(void)chooseSource:(id)sender{
    
    NSOpenPanel *oPanel = [NSOpenPanel openPanel];
    [oPanel setTitle:@"Choose Source"];
    [oPanel setAllowsMultipleSelection:NO];
    [oPanel setCanChooseDirectories:YES];
    
    [oPanel beginWithCompletionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            NSURL*  source = [[oPanel URLs] objectAtIndex:0];
            sourceURL = [source copy];
            //NSLog(@"Source is %@", sourceURL);
            sourcePath = [self urlToStringPath:sourceURL];
            [sourceField setStringValue:sourcePath];
        }
    }];
    
}



-(void)chooseTarget:(id)sender{
    
    NSSavePanel *sPanel = [NSSavePanel savePanel];
    [sPanel setTitle:@"Choose Target"];
    [sPanel setExtensionHidden:YES];
    
    [sPanel beginWithCompletionHandler:^(NSInteger result) {
        if (result == NSFileHandlingPanelOKButton) {
            NSURL*  target = [sPanel URL];
            targetURL = [target copy];
            //NSLog(@"Target is %@", targetURL);
            targetPath = [self urlToStringPath:targetURL];
            [targetField setStringValue:targetPath];
        }
    }];
    
}

-(void)createSymLink:(id)sender{
    
    if([sourcePath length] < 1){
        NSRunAlertPanel(@"Error", @"Please choose a source", @"Ok", nil, nil);
        return;
    }
    if([targetPath length] < 1){
        NSRunAlertPanel(@"Error", @"Please choose a target", @"Ok", nil, nil);
        return;
    }
    [symLinkTask setArguments:[NSArray arrayWithObjects:@"-s", sourcePath, targetPath, nil]];
    [symLinkTask launch];
    [symLinkTask waitUntilExit];
    NSRunAlertPanel(@"Done!", @"Created Symbolic Link ", @"Ok", nil, nil);
    
}


@end
