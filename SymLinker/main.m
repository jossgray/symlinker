//
//  main.m
//  SymLinker
//
//  Created by Joss Gray on 09/01/2012.
//  Copyright (c) 2012 none. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
